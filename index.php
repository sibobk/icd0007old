<?php

require_once "lib/sparrow.php";
require_once "lib/tpl.php";
require_once "contact.php";
// require_once 'contactList.php';
//$lines = file("data/contacts.txt");

class ContactController {
    public $db;
    public $errors = [];

    public function __construct(&$db)
    {
        $this->db = &$db;
        $this->db->using('Contact');
    }

    public function index($message = NULL) {
        $contacts = $this->db->find();
        if(!is_array($contacts))
            $contacts = array($contacts);

        // OK TODO: Message!
        print render_template(
            "views/main.html",
            ['$contacts' => $contacts,
                '$message' => $message ]
        );
    }

    public function edit($id = NULL) {
        // Initialize contact when creating new
        if($id == NULL) {
            $contact = new Contact();
        }

        // Find existing contact when editing
        else {
            // $contact = $this->db->join('phones', array('phone.contact_id' => 'contact.id'))->find($id);
            $contact = $this->db->find($id, "id");
        }

        print render_template("views/main.html",
            [
                '$edit' => true,
                '$contact' => $contact
            ]
        );
    }

    public function save($id = NULL, $params) {
        // Check if contact is valid
        $contact = new Contact();
        if(!empty($id))
            $contact->id = $id;

        $contact->firstName = $params['firstName'];
        $contact->lastName = $params['lastName'];
        $contact->phone1 = $params['phone1'];
        $contact->phone2 = $params['phone2'];
        $contact->phone3 = $params['phone3'];

        if($this->valid($contact))
        {
            $this->db->save($contact);

            // Redirect back to index with success message
            http_response_code(302);
            header("Location: ?cmd=list_page&message=saved");
            // NB! DIE DIE DIE, we shall not return any other content after header has been sent!
            die();
        }

        // Contact is invalid, reload form with prefilled data
        else {
            print render_template("views/main.html", [
                '$edit' => true,
                '$contact' => $contact,
                '$errors' => $this->errors
            ]
            );
        }
    }
    // Validate contact
    public function valid($contact) {

        $this->errors = array();

        if(empty($contact->firstName) || strlen($contact->firstName) < 2 || strlen($contact->firstName) > 50)
            $this->errors[] = "Eesnimi peab olema 2 kuni 50 märki!";

        if(empty($contact->lastName) || strlen($contact->lastName) < 2 || strlen($contact->lastName) > 50)
            $this->errors[] = "Perekonnanimi peab olema 2 kuni 50 märki!";

        return empty($this->errors);
    }
}


// Initiate SQL DAO library
$db = new Sparrow();

// Create connection based on PHP PDO connector
// $pdo = new PDO('sqlite:icd0007.sqlite3',null, null );
$pdo = new PDO('sqlite:icd0007.sqlite3', null, null);
$db->setDb($pdo);
// $db->setDb('pdosqlite://icd0007.sqlite3');
$db->show_sql = true;
// Seed
$db->sql('CREATE TABLE IF NOT EXISTS contact (
                    id INTEGER PRIMARY KEY, 
                    firstName VARCHAR(128), 
                    lastName VARCHAR(128),
                    phone1 VARCHAR(128),
                    phone2 VARCHAR(128),
                    phone3 VARCHAR(128)
                    )')->execute();


$controller = new ContactController($db);

// Route
// TODO: Initialize variables! Contact from get (ID) and post (content)!
$cmd = isset($_GET["cmd"]) ? $_GET["cmd"] : "list_page";

if(!empty($_REQUEST["person_id"]))
    $id = intval($_REQUEST["person_id"]);

else if(!empty($_REQUEST["id"]))
    $id = intval($_REQUEST["id"]);
else
    $id = NULL;

$keys = array(
    "firstName",
    "lastName",
    "phone1",
    "phone2",
    "phone3",
);
$params = array();

foreach($keys as $key)
    if(!empty($_REQUEST[$key]))
        $params[$key] = $_REQUEST[$key];
    else
        $params[$key] = "";

switch($cmd) {
    case "add_page":
        $controller->edit($id);
        break;

    case "edit_page":
        $controller->edit($id);
        break;

    case "save":
        $controller->save($id, $params);
        break;

    default:
        $controller->index();
}