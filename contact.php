<?php

class Contact{
    static $table = 'contact';
    static $id_field = 'id';

    public $id;
    public $firstName;
    public $lastName;
    public $phone1;
    public $phone2;
    public $phone3;

    /**
     * Contact constructor.
     */
    public function __construct()
    {
        $this->id = NULL;
        $this->firstName = "";
        $this->lastName = "";
        $this->phone1 = "";
        $this->phone2 = "";
        $this->phone3 = "";
    }
}
/*
class Phone {
    static $table = 'phone';
    static $id_field = 'id';

    public $id;
    public $order;
    public $contact_id;
    public $phone;

    public function __construct($id, $contact_id, $order, $phone)
    {
        $this->id = $id;
        $this->order = $order;
        $this->contact_id = $contact_id;
        $this->phone = $phone;
    }
}
*/