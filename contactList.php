<?php

const DATA_FILE = "data/contacts.txt";

function getContacts()
{
//    return file(DATA_FILE, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);
    $contacts = file(DATA_FILE, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);
    $contact_lines = [];
    foreach ($contacts as $contact) {

        $parts = explode(';', trim($contact));

        list($firstName, $lastName, $phone) = $parts;

        $contact_lines[] = new Contact($firstName, $lastName, $phone);
    }
    return $contact_lines;
}

function addContact($contact)
{
    if (isset($contact) && $contact != "") {
        $contacts = getContacts();
        if (!in_array($contact, $contacts)) {
            $firstName=$contact->firstName;
            $lastName=$contact->lastName;
            $phone=$contact->phone;

            file_put_contents(DATA_FILE, "$firstName;$lastName;$phone" . PHP_EOL, FILE_APPEND);
        }
    }
}

function deleteContact($contact)
{
    $contacts = getContacts();
    $index = array_search($contact, $contacts);
    if ($index!==false) {
        array_splice($contacts, $index, 1);
        file_put_contents(DATA_FILE, implode(PHP_EOL, $contacts));
        file_put_contents(DATA_FILE, PHP_EOL, FILE_APPEND);

    }else{
        print "<$contact> does not exist in the Contacts list!";
    }
}

function deleteAllContacts()
{
    file_put_contents(DATA_FILE, "");
}

//addContactItem("tegelt");

//deleteContactItem("tegelt");
//var_dump(getContactItems());